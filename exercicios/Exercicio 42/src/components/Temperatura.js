import React from 'react'

class Temperatura extends React.Component {

    constructor(props) {
        super(props)
        this.state = { temperatura: 0 }
    }

    aumentar = () => {
        this.setState({ temperatura: this.state.temperatura + 1 })
    }

    diminuir = () => {
        this.setState({ temperatura: this.state.temperatura - 1 })
    }


    render() {
        return (

            <div>Temperatura: {this.state.temperatura}
                <button onClick={this.aumentar} >+</button>
                <button onClick={this.diminuir}>-</button>
            </div>

        )
    }
}

export default Temperatura