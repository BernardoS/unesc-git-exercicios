import React from 'react'

class Velocidade extends React.Component {

    constructor(props) {
        super(props)
        this.state = { velocidade: 0 }
    }

    aumentar = () => {
        this.setState({ velocidade: this.state.velocidade + 1 })
    }

    diminuir = () => {
        this.setState({ velocidade: this.state.velocidade - 1 })
    }


    render() {
        return (

            <div>Velocidade: {this.state.velocidade}
                <button onClick={this.aumentar} >+</button>
                <button onClick={this.diminuir}>-</button>
            </div>

        )
    }
}

export default Velocidade