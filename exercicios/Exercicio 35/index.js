const express = require('express')
const app = express()
const port = 3000
const request = require('request')

app.get('/', (req, res) => {

    res.send('Digite "localhost:3000/gerador" para executar a função!')

})

app.get('/gerador', (req, res,next) => {

    var fs = require("fs")

    fs.readFile("gerador_jogador.json", "utf8", function (err, data) {
        if (err) {
            return console.log("Erro ao ler arquivo");
        }

        var objetoJson = JSON.parse(data)

        var nome = objetoJson["nome"][Math.floor(Math.random() * objetoJson.nome.length)]
        var sobrenome = objetoJson["sobrenome"][Math.floor(Math.random() * objetoJson.sobrenome.length)]
        var posicao = objetoJson["posicao"][Math.floor(Math.random() * objetoJson.posicao.length)]
        var clube = objetoJson["clube"][Math.floor(Math.random() * objetoJson.clube.length)]
        var idade = Math.floor(Math.random() * 24) + 17;

        res.json(nome + ' ' + sobrenome + ' é um futebolista brasileiro de ' + idade + ' anos que atua como ' + posicao + '.' + ' Atualmente defende o ' + clube)

    })

})

app.listen(port, () => console.log(`Rodando...`))