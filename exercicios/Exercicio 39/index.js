const express = require('express')
const app = express()
const port = 3000

// ROTAS
const router_usuarios = require('./routes/usuarios')
const router_oficina = require('./routes/oficina')
const router_local = require('./routes/local')

app.use(express.json())

app.use('/usuarios', router_usuarios)
app.use('/oficina', router_oficina)
app.use('/local', router_local)

app.listen(port, () => console.log(`Rodando...`))