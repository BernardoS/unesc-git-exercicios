/*

Implemente um pacote capaz de solicitar ao usuário uma sequência de
palavras. Ao final, o programa deve salvar todas as palavras em um
arquivo de texto (de preferência no formato JSON ). No entanto, o
arquivo não poderá conter palavras repetidas ou nulas.

*/

// LENDO ENTRADA

var rl = require('readline-sync')
var palavras = rl.question("Escreva uma sequencia de palavras: ")


// LENDO ARQUIVO DE TEXTO

const fs = require('fs')
var meuArquivo = fs.readFileSync("data.json", "utf8") // versão síncrona

if (meuArquivo.includes(palavras) || palavras == "") {

    console.log("Essa palavra já está contida no arquivo ou é nula, por isso não será adicionada!")

} else {

    // ESCREVENDO ARQUIVOS DE TEXTO

    var meuArquivo = fs.writeFileSync("data.json", palavras, { encoding: "utf8", flag: "a" }) // versão síncrona
    var meuArquivo = fs.writeFileSync("data.json", " ", { encoding: "utf8", flag: "a" }) // espaço para próxima palavra
    console.log("Palavras novas foram adicionadas ao arquivo: " + palavras)
    
// LENDO EM FORMA DE JSON

var palavra = meuArquivo.split(" ")
var json = { "palavras" : palavra }
console.log(json)


}