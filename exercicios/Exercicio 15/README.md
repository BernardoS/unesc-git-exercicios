Implemente duas classes de acordo com o código abaixo. 
A primeira deve ser uma classe chamada Gerador, que possui um método chamado gerarHTML, 
que retorna apenas um elemento div em branco, e um método chamado addLista, 
que armazena na classe um array com a lista de compras. 
A segunda classe chama-se GeradorLista e deve estender a primeira classe, 
alterando o método gerarHTML para gerar uma lista à partir de um array


classGerador{
//implementacao
}
classGeradorLista extends Gerador{
//implementação
}
var gerador = GeradorLista()
var listaCompras = ["shampoo", "frutas", "arroz"]
gerador.addLista(listaCompras)
gerador.geraHTML() // gera uma lista com os itens de listaCompras