var express = require('express')
var app = express()
const path = require('path');

app.use(express.static('api'))

app.get('/download', function (req, res) {

    var arquivo = path.resolve(__dirname)+"\\api\\img\\" + req.query.imagem

      res.download(arquivo)
})

app.listen(3000)
