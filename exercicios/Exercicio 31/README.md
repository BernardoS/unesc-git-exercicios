Implemente um formulário HTML capaz de inserir produtos em uma lista 
e uma aplicação usando nodejs capaz de receber estes itens e salvá-los em um arquivo de texto. 
O formulário HTML também deve ser capaz de mostrar os itens armazenados no servidor. 
Utilize o método GET para receber dados do formulário e fornecer os dados a serem inseridos.