var express = require('express')
var app = express()
const fs = require('fs')

app.use(express.static('api'))

app.get('/listar', function (req, res, ) {

  // Pega os produtos cadastrados no arquivo 'produtos.json'
  var arquivo = fs.readFileSync("produtos.txt", "utf8")
  var produtos = arquivo.split("\n")

  for (var i = 0; i < produtos.length; i++) {

    res.send(produtos)
  }

  res.redirect('/')

})

app.get('/inserir', function (req, res) {

  var arquivo = fs.readFileSync("produtos.txt", "utf8")
  
  if (arquivo == "") {
    fs.writeFileSync("produtos.txt", req.query.produto + " " + req.query.preco + " " + req.query.tipo, { encoding: "utf8", flag: "a" }) // versão síncrona
  } else {
    fs.writeFileSync("produtos.txt", "\n" + req.query.produto + " " + req.query.preco + " " + req.query.tipo, { encoding: "utf8", flag: "a" }) // versão síncrona

  }

  res.redirect('/listar')
})

app.listen(3000)