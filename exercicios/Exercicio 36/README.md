Crie uma nova rota chamada /jogador, que retorna um JSON com as mesmas chaves do exercício anterior. 

No entanto, o arquivo JSON deverá conter também um filtro que substitui a idade por um valor textual:



| Idade   |      Texto     
|----------|:-------------:
| 17 a 22 | novato 
| 23 a 28 | profissional   
| 29 a 34 | veterano 
| 35 a 40 | master 