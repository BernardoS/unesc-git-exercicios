Estilize o formulário do exercício anterior com CSS para que fique como o mockup. 
O CSS deve seguir as seguintes especificações: O fundo da página deve ser da cor #222
A família da fonte deve iniciar com Georgia A cor da fonte deve ser white 
O formulário deve estar posicionado ao centro da página e ocupar exatamente metade da tela.
Cada campo deve conter margem de 15px para baixo e uma cor de fundo #333 
Os inputs devem conter cor de fundo #444, sem bordas, ocupando 100% do espaço disponível.
O botão de registro deve ser da cor blueviolet, altura de 50px, negrito, sem borda e tamanho da fonte de 16px