/* Implemente um pacote capaz de ler um arquivo de texto cujo o caminho
é passado por parâmetro pelo usuário. O arquivo lido deverá conter
palavras que serão persistidas no banco LevelDB.
*/

// CRIAÇÃO DO BANCO DE DADOS

var level = require('level')
var db = level('BANCO') //Cria e/ou acessa o BD

// LENDO ENTRADA

var rl = require('readline-sync')
var path = rl.question("Digite o caminho completo do arquivo .txt:  ")

// LENDO ARQUIVO DE TEXTO

const fs = require('fs')
var meuArquivo = fs.readFileSync(path, "utf8") // versão síncrona


if (meuArquivo != "") {
    adiciona()
} else {
    console.log("Arquivo de texto vazio!")
}

// Persistência de dados no banco

function adiciona() {

    db.put("texto", meuArquivo, function (err) {
        if (!err) {
            console.log("Valor salvo com sucesso")
        }
    })

    db.close() // Fecha a conexão com o banco
}