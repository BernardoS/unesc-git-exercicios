var express = require('express')
var app = express()
const fs = require('fs')
var logado = false

// Se não estiver logado redireciona para login novamente
app.get('/sucesso.html', function (req, res, next) {
  if (logado == false) {
    res.redirect("/login.html")
  }
  next()
});

app.use(express.static('api'))


app.get('/login', function (req, res) {

  var usuario = req.query.usuario
  var senha = req.query.senha

  if (usuario === "root" && senha === "unesc2019") {
    logado = true
    res.redirect("/sucesso.html")
  } else {
    logado = false
    res.redirect("/login.html")

  }
})

app.get('/logout', function (req, res) {

  logado = false
  res.redirect("/login.html")

})

// Se não achar nenhum caminho válido, vai para página 404.html
app.use(function (req, res, next) {
  res.redirect("/404.html")
});


app.listen(3000)