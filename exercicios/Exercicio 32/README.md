# Crie três páginas HTML, com seu respectivo CSS:

- O primeiro arquivo deverá se chamar login.html, e deverá conter um formulário HTML contendo campos de entrada de usuário e senha.
- O segundo arquivo deverá se chamar sucesso.html, e deverá conter uma tela com uma mensagem de sucesso no login.
- O terceiro arquivo deverá se chamar 404.html, e deverá exibir uma mensagem de erro (recurso indisponível)

# Implemente uma aplicação usando nodejs de realizar a validação do login:

- O validador deverá ser implementado no lado do servidor e será responsável por validar apenas o usuário root com a senha unesc2019, enviando ao usuário a página sucesso.html.
- Caso a usuário ou senha não sejam validados, a página deverá retornar à página login.html, apresentando uma mensagem de erro (que deverá ser tratada por um script no lado do cliente)
- Para qualquer URI desconhecida, a aplicação deverá redirecionar para a página 404.html.

# Adicionais:

- Escreva um middleware que seja executado antes do express.static e impeça um usuário não logado de acessar a página sucesso.html.
- Utilize um botão de deslogar na página sucesso.html.